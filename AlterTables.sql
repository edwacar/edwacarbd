alter table vehiculos
  add constraint cliente_vehiculo_fk01
  foreign key (id_cliente)
  references clientes (id_cliente);
  
  alter table servicios
  add constraint cliente_servicio_fk01
  foreign key (id_cliente)
  references clientes (id_cliente);
