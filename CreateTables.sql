CREATE TABLE clientes (
	id_cliente serial PRIMARY KEY,
	nombre_cliente VARCHAR ( 500 ),
	cedula VARCHAR ( 100 ),
	telefono VARCHAR ( 50 ),
	email VARCHAR ( 255 ) UNIQUE,
	fec_creacion TIMESTAMP NOT NULL        
);


CREATE TABLE vehiculos(
	id_vehiculo serial PRIMARY KEY,
	modelo_marca VARCHAR ( 500 ),
	placa VARCHAR ( 100 ),	
	fec_creacion TIMESTAMP NOT null,
	id_cliente int not null
);

CREATE TABLE servicios(
	id_servicio serial PRIMARY KEY,
	diagnostico VARCHAR ( 2000 ) NOT null,
	fec_entrada TIMESTAMP not null,
	fec_salida TIMESTAMP not null,
	observaciones VARCHAR (250),
	fec_creacion TIMESTAMP NOT null,
	id_vehiculo int not null,
	id_cliente int not null);
